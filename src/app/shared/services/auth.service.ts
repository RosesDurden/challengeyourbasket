
import { Injectable, NgZone } from '@angular/core';
import { User } from "./user";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})

/** Service d'authentification Firebase */
export class AuthService {
  userData: any; // Save logged in user data
  erreurMessage:string = '';
  okMessage:string= "";

  constructor(
    public afs: AngularFirestore,   // Injecte le service Firestore
    public afAuth: AngularFireAuth, // Inject l'authService de Firebase
    public ngZone: NgZone // NgZone service pour supprimer tout ce qui est en dehors du scop
  ) {    
    /* Enregistrer les data de l'utilisateur dans le localstorage
     quand l'utilisateur est loggé et mettre à null quand il se déconnecte */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }

  // Se connetcer avec email/password
  SignIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          console.log('Logged in');
        });
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // S'inscrire avec email/password
  SignUp(email, password) {
    const input = document.getElementById('rgpdCheckbox') as HTMLInputElement;
    /** On vérifie que la case RGPD est bien cochée */
    if(input.checked == false){
      this.erreurMessage = 'Merci de cocher la case relative à l\'exploitation des données personnelles';
      document.getElementById('fireBaseError').innerText = this.erreurMessage;
      document.getElementById('fireBaseError').hidden=false;
      return;
    }
    else if(input.checked == true){
      return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* On appelle SendVerificaitonMail() quand un nouvel
        utilisateur s'enregistre */
        this.SetUserData(result.user);
        this.okMessage = 'Merci pour votre soutien, vous avez bien été enregistré ! ';
        document.getElementById('fireBaseError').style.visibility="hidden";
        document.getElementById('fireBaseOK').innerText = this.okMessage;
        document.getElementById('fireBaseOK').style.visibility="visible";
        //this.SendVerificationMail();
      }).catch((error) => {
        if(email == "" || password == "" )
        {
            this.erreurMessage = 'Merci de renseigner votre adresse mail et votre mot de passe';
        }
        else{
          switch(error.code){
            case 'auth/email-already-in-use':
              this.erreurMessage = 'Cette adresse email est déjà utilisée';
              break;
            case 'auth/weak-password':
              this.erreurMessage = 'Le mot de passe doit comporter au moins 6 caractères';
              break;
            case 'auth/invalid-email':
              this.erreurMessage = 'L\'adresse que vous avez renseigné est mal formatée.';
              break;
          }
        }
        document.getElementById('fireBaseError').innerText = this.erreurMessage;
        document.getElementById('fireBaseError').hidden=false;
      })
    }
  }

  // Envoie d'un mail de validation
  /*SendVerificationMail() {
    return this.userData.user.sendEmailVerification()
    .then(() => {
      console.log('Email envoyé pour vérification');
    })
  }*/

  // En cas de mot de passe oublié
  /*ForgotPassword(passwordResetEmail) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Password reset email sent, check your inbox.');
    }).catch((error) => {
      window.alert(error)
    })
  }*/

  // Vrai quand l'utilisateur est déjà authentifié sur le site et que l'email est vérifié
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // S'enregistrer avec Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  // Logique d'authentification pour les providers d'authentification
  AuthLogin(provider) {
    return this.afAuth.signInWithPopup(provider)
    .then((result) => {
       this.ngZone.run(() => {
          console.log('OK');
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }

  /* Paramètrage des données utilisateurs quand ils se connectent avec email/password */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Déconnection
  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      console.log('Déconnecté');
    })
  }

}