import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  //injection du service dans le composant
  constructor(public authService:AuthService){}

  title = 'Challenge Your Basket';
}
